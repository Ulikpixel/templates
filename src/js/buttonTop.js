//----- button top -----//
const btnTop = document.querySelector('.btn__top');

//  button top show
window.addEventListener('scroll', () => {
    window.pageYOffset > 1000 ? btnTop.classList.add('btn__active') : btnTop.classList.remove('btn__active');
});

// button click top
btnTop.addEventListener('click', () => window.scroll(0, 0));