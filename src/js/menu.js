//----- navbar + burger active -----//
const burger      = document.querySelector('.header__burger'),
      navbar      = document.querySelector('.header__navbar'),
      popupNavbar = document.querySelector('.header__popup');
//function open menu
const openMenu = () => {
    burger.classList.toggle('burger__active');
    navbar.classList.toggle('navbar__active');
    popupNavbar.classList.toggle('popup__active');
}

popupNavbar.addEventListener('click', openMenu);
burger.addEventListener('click', openMenu);