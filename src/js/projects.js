//----- media window -----//
const media = condition => (window.matchMedia(condition).matches);

//----- projects slider -----//
const projectTrack = document.querySelector('.projects__track'),
      projectPrev  = document.querySelector('.projects__prev'),
      projectNext  = document.querySelector('.projects__next');
// position track
let projectPs = 0;
//next
const nextProject = () => {
    if(media("(min-width: 1200px)")){
        projectPs == 1140 * 2 ? projectPs = 0 : projectPs += 1140;
    }
    else if(media("(min-width: 576px)")){
        projectPs == 570 * 5 ? projectPs = 0 : projectPs += 570;
    }
    else{
        projectPs == 300 * 5 ? projectPs = 0 : projectPs += 300;
    }
    projectTrack.style.transform = `translate(-${projectPs}px)`;
}
//prev
const prevProject = () => {
    if(media("(min-width: 1200px)")){
        projectPs == 0 ? projectPs = 1140 * 2 : projectPs -= 1140;
    }
    else if(media("(min-width: 576px)")){
        projectPs == 0 ? projectPs = 570 * 5 : projectPs -= 570;
    }
    else{
        projectPs == 0 ? projectPs = 300 * 5 : projectPs -= 300;
    }
    projectTrack.style.transform = `translate(-${projectPs}px)`;
}

projectNext.addEventListener('click', nextProject);
projectPrev.addEventListener('click', prevProject);
