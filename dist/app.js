/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/index.js":
/*!**********************!*\
  !*** ./src/index.js ***!
  \**********************/
/*! no static exports found */
/***/ (function(module, exports) {



/***/ }),

/***/ "./src/js/buttonTop.js":
/*!*****************************!*\
  !*** ./src/js/buttonTop.js ***!
  \*****************************/
/*! no static exports found */
/***/ (function(module, exports) {

//----- button top -----//
const btnTop = document.querySelector('.btn__top');

//  button top show
window.addEventListener('scroll', () => {
    window.pageYOffset > 1000 ? btnTop.classList.add('btn__active') : btnTop.classList.remove('btn__active');
});

// button click top
btnTop.addEventListener('click', () => window.scroll(0, 0));

/***/ }),

/***/ "./src/js/menu.js":
/*!************************!*\
  !*** ./src/js/menu.js ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

//----- navbar + burger active -----//
const burger      = document.querySelector('.header__burger'),
      navbar      = document.querySelector('.header__navbar'),
      popupNavbar = document.querySelector('.header__popup');
//function open menu
const openMenu = () => {
    burger.classList.toggle('burger__active');
    navbar.classList.toggle('navbar__active');
    popupNavbar.classList.toggle('popup__active');
}

popupNavbar.addEventListener('click', openMenu);
burger.addEventListener('click', openMenu);

/***/ }),

/***/ "./src/js/projects.js":
/*!****************************!*\
  !*** ./src/js/projects.js ***!
  \****************************/
/*! no static exports found */
/***/ (function(module, exports) {

//----- media window -----//
const media = condition => (window.matchMedia(condition).matches);

//----- projects slider -----//
const projectTrack = document.querySelector('.projects__track'),
      projectPrev  = document.querySelector('.projects__prev'),
      projectNext  = document.querySelector('.projects__next');
// position track
let projectPs = 0;
//next
const nextProject = () => {
    if(media("(min-width: 1200px)")){
        projectPs == 1140 * 2 ? projectPs = 0 : projectPs += 1140;
    }
    else if(media("(min-width: 576px)")){
        projectPs == 570 * 5 ? projectPs = 0 : projectPs += 570;
    }
    else{
        projectPs == 300 * 5 ? projectPs = 0 : projectPs += 300;
    }
    projectTrack.style.transform = `translate(-${projectPs}px)`;
}
//prev
const prevProject = () => {
    if(media("(min-width: 1200px)")){
        projectPs == 0 ? projectPs = 1140 * 2 : projectPs -= 1140;
    }
    else if(media("(min-width: 576px)")){
        projectPs == 0 ? projectPs = 570 * 5 : projectPs -= 570;
    }
    else{
        projectPs == 0 ? projectPs = 300 * 5 : projectPs -= 300;
    }
    projectTrack.style.transform = `translate(-${projectPs}px)`;
}

projectNext.addEventListener('click', nextProject);
projectPrev.addEventListener('click', prevProject);


/***/ }),

/***/ 0:
/*!****************************************************************************************!*\
  !*** multi ./src/index.js ./src/js/buttonTop.js ./src/js/menu.js ./src/js/projects.js ***!
  \****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(/*! D:\gulpLayout\src\index.js */"./src/index.js");
__webpack_require__(/*! D:\gulpLayout\src\js\buttonTop.js */"./src/js/buttonTop.js");
__webpack_require__(/*! D:\gulpLayout\src\js\menu.js */"./src/js/menu.js");
module.exports = __webpack_require__(/*! D:\gulpLayout\src\js\projects.js */"./src/js/projects.js");


/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAiLCJ3ZWJwYWNrOi8vLy4vc3JjL2pzL2J1dHRvblRvcC5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvanMvbWVudS5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvanMvcHJvamVjdHMuanMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtRQUFBO1FBQ0E7O1FBRUE7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTs7UUFFQTtRQUNBOztRQUVBO1FBQ0E7O1FBRUE7UUFDQTtRQUNBOzs7UUFHQTtRQUNBOztRQUVBO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0EsMENBQTBDLGdDQUFnQztRQUMxRTtRQUNBOztRQUVBO1FBQ0E7UUFDQTtRQUNBLHdEQUF3RCxrQkFBa0I7UUFDMUU7UUFDQSxpREFBaUQsY0FBYztRQUMvRDs7UUFFQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0EseUNBQXlDLGlDQUFpQztRQUMxRSxnSEFBZ0gsbUJBQW1CLEVBQUU7UUFDckk7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7UUFDQSwyQkFBMkIsMEJBQTBCLEVBQUU7UUFDdkQsaUNBQWlDLGVBQWU7UUFDaEQ7UUFDQTtRQUNBOztRQUVBO1FBQ0Esc0RBQXNELCtEQUErRDs7UUFFckg7UUFDQTs7O1FBR0E7UUFDQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNsRkE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxDQUFDOztBQUVEO0FBQ0EsNEQ7Ozs7Ozs7Ozs7O0FDVEE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQSwyQzs7Ozs7Ozs7Ozs7QUNaQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpREFBaUQsVUFBVTtBQUMzRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpREFBaUQsVUFBVTtBQUMzRDs7QUFFQTtBQUNBIiwiZmlsZSI6ImFwcC5qcyIsInNvdXJjZXNDb250ZW50IjpbIiBcdC8vIFRoZSBtb2R1bGUgY2FjaGVcbiBcdHZhciBpbnN0YWxsZWRNb2R1bGVzID0ge307XG5cbiBcdC8vIFRoZSByZXF1aXJlIGZ1bmN0aW9uXG4gXHRmdW5jdGlvbiBfX3dlYnBhY2tfcmVxdWlyZV9fKG1vZHVsZUlkKSB7XG5cbiBcdFx0Ly8gQ2hlY2sgaWYgbW9kdWxlIGlzIGluIGNhY2hlXG4gXHRcdGlmKGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdKSB7XG4gXHRcdFx0cmV0dXJuIGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdLmV4cG9ydHM7XG4gXHRcdH1cbiBcdFx0Ly8gQ3JlYXRlIGEgbmV3IG1vZHVsZSAoYW5kIHB1dCBpdCBpbnRvIHRoZSBjYWNoZSlcbiBcdFx0dmFyIG1vZHVsZSA9IGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdID0ge1xuIFx0XHRcdGk6IG1vZHVsZUlkLFxuIFx0XHRcdGw6IGZhbHNlLFxuIFx0XHRcdGV4cG9ydHM6IHt9XG4gXHRcdH07XG5cbiBcdFx0Ly8gRXhlY3V0ZSB0aGUgbW9kdWxlIGZ1bmN0aW9uXG4gXHRcdG1vZHVsZXNbbW9kdWxlSWRdLmNhbGwobW9kdWxlLmV4cG9ydHMsIG1vZHVsZSwgbW9kdWxlLmV4cG9ydHMsIF9fd2VicGFja19yZXF1aXJlX18pO1xuXG4gXHRcdC8vIEZsYWcgdGhlIG1vZHVsZSBhcyBsb2FkZWRcbiBcdFx0bW9kdWxlLmwgPSB0cnVlO1xuXG4gXHRcdC8vIFJldHVybiB0aGUgZXhwb3J0cyBvZiB0aGUgbW9kdWxlXG4gXHRcdHJldHVybiBtb2R1bGUuZXhwb3J0cztcbiBcdH1cblxuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZXMgb2JqZWN0IChfX3dlYnBhY2tfbW9kdWxlc19fKVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5tID0gbW9kdWxlcztcblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGUgY2FjaGVcbiBcdF9fd2VicGFja19yZXF1aXJlX18uYyA9IGluc3RhbGxlZE1vZHVsZXM7XG5cbiBcdC8vIGRlZmluZSBnZXR0ZXIgZnVuY3Rpb24gZm9yIGhhcm1vbnkgZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kID0gZnVuY3Rpb24oZXhwb3J0cywgbmFtZSwgZ2V0dGVyKSB7XG4gXHRcdGlmKCFfX3dlYnBhY2tfcmVxdWlyZV9fLm8oZXhwb3J0cywgbmFtZSkpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgbmFtZSwgeyBlbnVtZXJhYmxlOiB0cnVlLCBnZXQ6IGdldHRlciB9KTtcbiBcdFx0fVxuIFx0fTtcblxuIFx0Ly8gZGVmaW5lIF9fZXNNb2R1bGUgb24gZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5yID0gZnVuY3Rpb24oZXhwb3J0cykge1xuIFx0XHRpZih0eXBlb2YgU3ltYm9sICE9PSAndW5kZWZpbmVkJyAmJiBTeW1ib2wudG9TdHJpbmdUYWcpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgU3ltYm9sLnRvU3RyaW5nVGFnLCB7IHZhbHVlOiAnTW9kdWxlJyB9KTtcbiBcdFx0fVxuIFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgJ19fZXNNb2R1bGUnLCB7IHZhbHVlOiB0cnVlIH0pO1xuIFx0fTtcblxuIFx0Ly8gY3JlYXRlIGEgZmFrZSBuYW1lc3BhY2Ugb2JqZWN0XG4gXHQvLyBtb2RlICYgMTogdmFsdWUgaXMgYSBtb2R1bGUgaWQsIHJlcXVpcmUgaXRcbiBcdC8vIG1vZGUgJiAyOiBtZXJnZSBhbGwgcHJvcGVydGllcyBvZiB2YWx1ZSBpbnRvIHRoZSBuc1xuIFx0Ly8gbW9kZSAmIDQ6IHJldHVybiB2YWx1ZSB3aGVuIGFscmVhZHkgbnMgb2JqZWN0XG4gXHQvLyBtb2RlICYgOHwxOiBiZWhhdmUgbGlrZSByZXF1aXJlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnQgPSBmdW5jdGlvbih2YWx1ZSwgbW9kZSkge1xuIFx0XHRpZihtb2RlICYgMSkgdmFsdWUgPSBfX3dlYnBhY2tfcmVxdWlyZV9fKHZhbHVlKTtcbiBcdFx0aWYobW9kZSAmIDgpIHJldHVybiB2YWx1ZTtcbiBcdFx0aWYoKG1vZGUgJiA0KSAmJiB0eXBlb2YgdmFsdWUgPT09ICdvYmplY3QnICYmIHZhbHVlICYmIHZhbHVlLl9fZXNNb2R1bGUpIHJldHVybiB2YWx1ZTtcbiBcdFx0dmFyIG5zID0gT2JqZWN0LmNyZWF0ZShudWxsKTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5yKG5zKTtcbiBcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KG5zLCAnZGVmYXVsdCcsIHsgZW51bWVyYWJsZTogdHJ1ZSwgdmFsdWU6IHZhbHVlIH0pO1xuIFx0XHRpZihtb2RlICYgMiAmJiB0eXBlb2YgdmFsdWUgIT0gJ3N0cmluZycpIGZvcih2YXIga2V5IGluIHZhbHVlKSBfX3dlYnBhY2tfcmVxdWlyZV9fLmQobnMsIGtleSwgZnVuY3Rpb24oa2V5KSB7IHJldHVybiB2YWx1ZVtrZXldOyB9LmJpbmQobnVsbCwga2V5KSk7XG4gXHRcdHJldHVybiBucztcbiBcdH07XG5cbiBcdC8vIGdldERlZmF1bHRFeHBvcnQgZnVuY3Rpb24gZm9yIGNvbXBhdGliaWxpdHkgd2l0aCBub24taGFybW9ueSBtb2R1bGVzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm4gPSBmdW5jdGlvbihtb2R1bGUpIHtcbiBcdFx0dmFyIGdldHRlciA9IG1vZHVsZSAmJiBtb2R1bGUuX19lc01vZHVsZSA/XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0RGVmYXVsdCgpIHsgcmV0dXJuIG1vZHVsZVsnZGVmYXVsdCddOyB9IDpcbiBcdFx0XHRmdW5jdGlvbiBnZXRNb2R1bGVFeHBvcnRzKCkgeyByZXR1cm4gbW9kdWxlOyB9O1xuIFx0XHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQoZ2V0dGVyLCAnYScsIGdldHRlcik7XG4gXHRcdHJldHVybiBnZXR0ZXI7XG4gXHR9O1xuXG4gXHQvLyBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGxcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubyA9IGZ1bmN0aW9uKG9iamVjdCwgcHJvcGVydHkpIHsgcmV0dXJuIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChvYmplY3QsIHByb3BlcnR5KTsgfTtcblxuIFx0Ly8gX193ZWJwYWNrX3B1YmxpY19wYXRoX19cbiBcdF9fd2VicGFja19yZXF1aXJlX18ucCA9IFwiXCI7XG5cblxuIFx0Ly8gTG9hZCBlbnRyeSBtb2R1bGUgYW5kIHJldHVybiBleHBvcnRzXG4gXHRyZXR1cm4gX193ZWJwYWNrX3JlcXVpcmVfXyhfX3dlYnBhY2tfcmVxdWlyZV9fLnMgPSAwKTtcbiIsIi8vLS0tLS0gYnV0dG9uIHRvcCAtLS0tLS8vXHJcbmNvbnN0IGJ0blRvcCA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJy5idG5fX3RvcCcpO1xyXG5cclxuLy8gIGJ1dHRvbiB0b3Agc2hvd1xyXG53aW5kb3cuYWRkRXZlbnRMaXN0ZW5lcignc2Nyb2xsJywgKCkgPT4ge1xyXG4gICAgd2luZG93LnBhZ2VZT2Zmc2V0ID4gMTAwMCA/IGJ0blRvcC5jbGFzc0xpc3QuYWRkKCdidG5fX2FjdGl2ZScpIDogYnRuVG9wLmNsYXNzTGlzdC5yZW1vdmUoJ2J0bl9fYWN0aXZlJyk7XHJcbn0pO1xyXG5cclxuLy8gYnV0dG9uIGNsaWNrIHRvcFxyXG5idG5Ub3AuYWRkRXZlbnRMaXN0ZW5lcignY2xpY2snLCAoKSA9PiB3aW5kb3cuc2Nyb2xsKDAsIDApKTsiLCIvLy0tLS0tIG5hdmJhciArIGJ1cmdlciBhY3RpdmUgLS0tLS0vL1xyXG5jb25zdCBidXJnZXIgICAgICA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJy5oZWFkZXJfX2J1cmdlcicpLFxyXG4gICAgICBuYXZiYXIgICAgICA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJy5oZWFkZXJfX25hdmJhcicpLFxyXG4gICAgICBwb3B1cE5hdmJhciA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJy5oZWFkZXJfX3BvcHVwJyk7XHJcbi8vZnVuY3Rpb24gb3BlbiBtZW51XHJcbmNvbnN0IG9wZW5NZW51ID0gKCkgPT4ge1xyXG4gICAgYnVyZ2VyLmNsYXNzTGlzdC50b2dnbGUoJ2J1cmdlcl9fYWN0aXZlJyk7XHJcbiAgICBuYXZiYXIuY2xhc3NMaXN0LnRvZ2dsZSgnbmF2YmFyX19hY3RpdmUnKTtcclxuICAgIHBvcHVwTmF2YmFyLmNsYXNzTGlzdC50b2dnbGUoJ3BvcHVwX19hY3RpdmUnKTtcclxufVxyXG5cclxucG9wdXBOYXZiYXIuYWRkRXZlbnRMaXN0ZW5lcignY2xpY2snLCBvcGVuTWVudSk7XHJcbmJ1cmdlci5hZGRFdmVudExpc3RlbmVyKCdjbGljaycsIG9wZW5NZW51KTsiLCIvLy0tLS0tIG1lZGlhIHdpbmRvdyAtLS0tLS8vXHJcbmNvbnN0IG1lZGlhID0gY29uZGl0aW9uID0+ICh3aW5kb3cubWF0Y2hNZWRpYShjb25kaXRpb24pLm1hdGNoZXMpO1xyXG5cclxuLy8tLS0tLSBwcm9qZWN0cyBzbGlkZXIgLS0tLS0vL1xyXG5jb25zdCBwcm9qZWN0VHJhY2sgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcucHJvamVjdHNfX3RyYWNrJyksXHJcbiAgICAgIHByb2plY3RQcmV2ICA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJy5wcm9qZWN0c19fcHJldicpLFxyXG4gICAgICBwcm9qZWN0TmV4dCAgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcucHJvamVjdHNfX25leHQnKTtcclxuLy8gcG9zaXRpb24gdHJhY2tcclxubGV0IHByb2plY3RQcyA9IDA7XHJcbi8vbmV4dFxyXG5jb25zdCBuZXh0UHJvamVjdCA9ICgpID0+IHtcclxuICAgIGlmKG1lZGlhKFwiKG1pbi13aWR0aDogMTIwMHB4KVwiKSl7XHJcbiAgICAgICAgcHJvamVjdFBzID09IDExNDAgKiAyID8gcHJvamVjdFBzID0gMCA6IHByb2plY3RQcyArPSAxMTQwO1xyXG4gICAgfVxyXG4gICAgZWxzZSBpZihtZWRpYShcIihtaW4td2lkdGg6IDU3NnB4KVwiKSl7XHJcbiAgICAgICAgcHJvamVjdFBzID09IDU3MCAqIDUgPyBwcm9qZWN0UHMgPSAwIDogcHJvamVjdFBzICs9IDU3MDtcclxuICAgIH1cclxuICAgIGVsc2V7XHJcbiAgICAgICAgcHJvamVjdFBzID09IDMwMCAqIDUgPyBwcm9qZWN0UHMgPSAwIDogcHJvamVjdFBzICs9IDMwMDtcclxuICAgIH1cclxuICAgIHByb2plY3RUcmFjay5zdHlsZS50cmFuc2Zvcm0gPSBgdHJhbnNsYXRlKC0ke3Byb2plY3RQc31weClgO1xyXG59XHJcbi8vcHJldlxyXG5jb25zdCBwcmV2UHJvamVjdCA9ICgpID0+IHtcclxuICAgIGlmKG1lZGlhKFwiKG1pbi13aWR0aDogMTIwMHB4KVwiKSl7XHJcbiAgICAgICAgcHJvamVjdFBzID09IDAgPyBwcm9qZWN0UHMgPSAxMTQwICogMiA6IHByb2plY3RQcyAtPSAxMTQwO1xyXG4gICAgfVxyXG4gICAgZWxzZSBpZihtZWRpYShcIihtaW4td2lkdGg6IDU3NnB4KVwiKSl7XHJcbiAgICAgICAgcHJvamVjdFBzID09IDAgPyBwcm9qZWN0UHMgPSA1NzAgKiA1IDogcHJvamVjdFBzIC09IDU3MDtcclxuICAgIH1cclxuICAgIGVsc2V7XHJcbiAgICAgICAgcHJvamVjdFBzID09IDAgPyBwcm9qZWN0UHMgPSAzMDAgKiA1IDogcHJvamVjdFBzIC09IDMwMDtcclxuICAgIH1cclxuICAgIHByb2plY3RUcmFjay5zdHlsZS50cmFuc2Zvcm0gPSBgdHJhbnNsYXRlKC0ke3Byb2plY3RQc31weClgO1xyXG59XHJcblxyXG5wcm9qZWN0TmV4dC5hZGRFdmVudExpc3RlbmVyKCdjbGljaycsIG5leHRQcm9qZWN0KTtcclxucHJvamVjdFByZXYuYWRkRXZlbnRMaXN0ZW5lcignY2xpY2snLCBwcmV2UHJvamVjdCk7XHJcbiJdLCJwcmVFeGlzdGluZ0NvbW1lbnQiOiIvLyMgc291cmNlTWFwcGluZ1VSTD1kYXRhOmFwcGxpY2F0aW9uL2pzb247Y2hhcnNldD11dGYtODtiYXNlNjQsZXlKMlpYSnphVzl1SWpvekxDSnpiM1Z5WTJWeklqcGJJbmRsWW5CaFkyczZMeTh2ZDJWaWNHRmpheTlpYjI5MGMzUnlZWEFpTENKM1pXSndZV05yT2k4dkx5NHZjM0pqTDJwekwySjFkSFJ2YmxSdmNDNXFjeUlzSW5kbFluQmhZMnM2THk4dkxpOXpjbU12YW5NdmJXVnVkUzVxY3lJc0luZGxZbkJoWTJzNkx5OHZMaTl6Y21NdmFuTXZjSEp2YW1WamRITXVhbk1pWFN3aWJtRnRaWE1pT2x0ZExDSnRZWEJ3YVc1bmN5STZJanRSUVVGQk8xRkJRMEU3TzFGQlJVRTdVVUZEUVRzN1VVRkZRVHRSUVVOQk8xRkJRMEU3VVVGRFFUdFJRVU5CTzFGQlEwRTdVVUZEUVR0UlFVTkJPMUZCUTBFN1VVRkRRVHM3VVVGRlFUdFJRVU5CT3p0UlFVVkJPMUZCUTBFN08xRkJSVUU3VVVGRFFUdFJRVU5CT3pzN1VVRkhRVHRSUVVOQk96dFJRVVZCTzFGQlEwRTdPMUZCUlVFN1VVRkRRVHRSUVVOQk8xRkJRMEVzTUVOQlFUQkRMR2REUVVGblF6dFJRVU14UlR0UlFVTkJPenRSUVVWQk8xRkJRMEU3VVVGRFFUdFJRVU5CTEhkRVFVRjNSQ3hyUWtGQmEwSTdVVUZETVVVN1VVRkRRU3hwUkVGQmFVUXNZMEZCWXp0UlFVTXZSRHM3VVVGRlFUdFJRVU5CTzFGQlEwRTdVVUZEUVR0UlFVTkJPMUZCUTBFN1VVRkRRVHRSUVVOQk8xRkJRMEU3VVVGRFFUdFJRVU5CTzFGQlEwRXNlVU5CUVhsRExHbERRVUZwUXp0UlFVTXhSU3huU0VGQlowZ3NiVUpCUVcxQ0xFVkJRVVU3VVVGRGNrazdVVUZEUVRzN1VVRkZRVHRSUVVOQk8xRkJRMEU3VVVGRFFTd3lRa0ZCTWtJc01FSkJRVEJDTEVWQlFVVTdVVUZEZGtRc2FVTkJRV2xETEdWQlFXVTdVVUZEYUVRN1VVRkRRVHRSUVVOQk96dFJRVVZCTzFGQlEwRXNjMFJCUVhORUxDdEVRVUVyUkRzN1VVRkZja2c3VVVGRFFUczdPMUZCUjBFN1VVRkRRVHM3T3pzN096czdPenM3T3pzN096czdPenM3T3pzN1FVTnNSa0U3UVVGRFFUczdRVUZGUVR0QlFVTkJPMEZCUTBFN1FVRkRRU3hEUVVGRE96dEJRVVZFTzBGQlEwRXNORVE3T3pzN096czdPenM3TzBGRFZFRTdRVUZEUVR0QlFVTkJPMEZCUTBFN1FVRkRRVHRCUVVOQk8wRkJRMEU3UVVGRFFUdEJRVU5CTzBGQlEwRTdPMEZCUlVFN1FVRkRRU3d5UXpzN096czdPenM3T3pzN1FVTmFRVHRCUVVOQk96dEJRVVZCTzBGQlEwRTdRVUZEUVR0QlFVTkJPMEZCUTBFN1FVRkRRVHRCUVVOQk8wRkJRMEU3UVVGRFFUdEJRVU5CTzBGQlEwRTdRVUZEUVR0QlFVTkJPMEZCUTBFN1FVRkRRVHRCUVVOQk8wRkJRMEU3UVVGRFFTeHBSRUZCYVVRc1ZVRkJWVHRCUVVNelJEdEJRVU5CTzBGQlEwRTdRVUZEUVR0QlFVTkJPMEZCUTBFN1FVRkRRVHRCUVVOQk8wRkJRMEU3UVVGRFFUdEJRVU5CTzBGQlEwRTdRVUZEUVN4cFJFRkJhVVFzVlVGQlZUdEJRVU16UkRzN1FVRkZRVHRCUVVOQklpd2labWxzWlNJNklqSTJNMlkzTTJKbFlqZ3lPVFl6WVdGak5UTTFMbXB6SWl3aWMyOTFjbU5sYzBOdmJuUmxiblFpT2xzaUlGeDBMeThnVkdobElHMXZaSFZzWlNCallXTm9aVnh1SUZ4MGRtRnlJR2x1YzNSaGJHeGxaRTF2WkhWc1pYTWdQU0I3ZlR0Y2JseHVJRngwTHk4Z1ZHaGxJSEpsY1hWcGNtVWdablZ1WTNScGIyNWNiaUJjZEdaMWJtTjBhVzl1SUY5ZmQyVmljR0ZqYTE5eVpYRjFhWEpsWDE4b2JXOWtkV3hsU1dRcElIdGNibHh1SUZ4MFhIUXZMeUJEYUdWamF5QnBaaUJ0YjJSMWJHVWdhWE1nYVc0Z1kyRmphR1ZjYmlCY2RGeDBhV1lvYVc1emRHRnNiR1ZrVFc5a2RXeGxjMXR0YjJSMWJHVkpaRjBwSUh0Y2JpQmNkRngwWEhSeVpYUjFjbTRnYVc1emRHRnNiR1ZrVFc5a2RXeGxjMXR0YjJSMWJHVkpaRjB1Wlhod2IzSjBjenRjYmlCY2RGeDBmVnh1SUZ4MFhIUXZMeUJEY21WaGRHVWdZU0J1WlhjZ2JXOWtkV3hsSUNoaGJtUWdjSFYwSUdsMElHbHVkRzhnZEdobElHTmhZMmhsS1Z4dUlGeDBYSFIyWVhJZ2JXOWtkV3hsSUQwZ2FXNXpkR0ZzYkdWa1RXOWtkV3hsYzF0dGIyUjFiR1ZKWkYwZ1BTQjdYRzRnWEhSY2RGeDBhVG9nYlc5a2RXeGxTV1FzWEc0Z1hIUmNkRngwYkRvZ1ptRnNjMlVzWEc0Z1hIUmNkRngwWlhod2IzSjBjem9nZTMxY2JpQmNkRngwZlR0Y2JseHVJRngwWEhRdkx5QkZlR1ZqZFhSbElIUm9aU0J0YjJSMWJHVWdablZ1WTNScGIyNWNiaUJjZEZ4MGJXOWtkV3hsYzF0dGIyUjFiR1ZKWkYwdVkyRnNiQ2h0YjJSMWJHVXVaWGh3YjNKMGN5d2diVzlrZFd4bExDQnRiMlIxYkdVdVpYaHdiM0owY3l3Z1gxOTNaV0p3WVdOclgzSmxjWFZwY21WZlh5azdYRzVjYmlCY2RGeDBMeThnUm14aFp5QjBhR1VnYlc5a2RXeGxJR0Z6SUd4dllXUmxaRnh1SUZ4MFhIUnRiMlIxYkdVdWJDQTlJSFJ5ZFdVN1hHNWNiaUJjZEZ4MEx5OGdVbVYwZFhKdUlIUm9aU0JsZUhCdmNuUnpJRzltSUhSb1pTQnRiMlIxYkdWY2JpQmNkRngwY21WMGRYSnVJRzF2WkhWc1pTNWxlSEJ2Y25Sek8xeHVJRngwZlZ4dVhHNWNiaUJjZEM4dklHVjRjRzl6WlNCMGFHVWdiVzlrZFd4bGN5QnZZbXBsWTNRZ0tGOWZkMlZpY0dGamExOXRiMlIxYkdWelgxOHBYRzRnWEhSZlgzZGxZbkJoWTJ0ZmNtVnhkV2x5WlY5ZkxtMGdQU0J0YjJSMWJHVnpPMXh1WEc0Z1hIUXZMeUJsZUhCdmMyVWdkR2hsSUcxdlpIVnNaU0JqWVdOb1pWeHVJRngwWDE5M1pXSndZV05yWDNKbGNYVnBjbVZmWHk1aklEMGdhVzV6ZEdGc2JHVmtUVzlrZFd4bGN6dGNibHh1SUZ4MEx5OGdaR1ZtYVc1bElHZGxkSFJsY2lCbWRXNWpkR2x2YmlCbWIzSWdhR0Z5Ylc5dWVTQmxlSEJ2Y25SelhHNGdYSFJmWDNkbFluQmhZMnRmY21WeGRXbHlaVjlmTG1RZ1BTQm1kVzVqZEdsdmJpaGxlSEJ2Y25SekxDQnVZVzFsTENCblpYUjBaWElwSUh0Y2JpQmNkRngwYVdZb0lWOWZkMlZpY0dGamExOXlaWEYxYVhKbFgxOHVieWhsZUhCdmNuUnpMQ0J1WVcxbEtTa2dlMXh1SUZ4MFhIUmNkRTlpYW1WamRDNWtaV1pwYm1WUWNtOXdaWEowZVNobGVIQnZjblJ6TENCdVlXMWxMQ0I3SUdWdWRXMWxjbUZpYkdVNklIUnlkV1VzSUdkbGREb2daMlYwZEdWeUlIMHBPMXh1SUZ4MFhIUjlYRzRnWEhSOU8xeHVYRzRnWEhRdkx5QmtaV1pwYm1VZ1gxOWxjMDF2WkhWc1pTQnZiaUJsZUhCdmNuUnpYRzRnWEhSZlgzZGxZbkJoWTJ0ZmNtVnhkV2x5WlY5ZkxuSWdQU0JtZFc1amRHbHZiaWhsZUhCdmNuUnpLU0I3WEc0Z1hIUmNkR2xtS0hSNWNHVnZaaUJUZVcxaWIyd2dJVDA5SUNkMWJtUmxabWx1WldRbklDWW1JRk41YldKdmJDNTBiMU4wY21sdVoxUmhaeWtnZTF4dUlGeDBYSFJjZEU5aWFtVmpkQzVrWldacGJtVlFjbTl3WlhKMGVTaGxlSEJ2Y25SekxDQlRlVzFpYjJ3dWRHOVRkSEpwYm1kVVlXY3NJSHNnZG1Gc2RXVTZJQ2ROYjJSMWJHVW5JSDBwTzF4dUlGeDBYSFI5WEc0Z1hIUmNkRTlpYW1WamRDNWtaV1pwYm1WUWNtOXdaWEowZVNobGVIQnZjblJ6TENBblgxOWxjMDF2WkhWc1pTY3NJSHNnZG1Gc2RXVTZJSFJ5ZFdVZ2ZTazdYRzRnWEhSOU8xeHVYRzRnWEhRdkx5QmpjbVZoZEdVZ1lTQm1ZV3RsSUc1aGJXVnpjR0ZqWlNCdlltcGxZM1JjYmlCY2RDOHZJRzF2WkdVZ0ppQXhPaUIyWVd4MVpTQnBjeUJoSUcxdlpIVnNaU0JwWkN3Z2NtVnhkV2x5WlNCcGRGeHVJRngwTHk4Z2JXOWtaU0FtSURJNklHMWxjbWRsSUdGc2JDQndjbTl3WlhKMGFXVnpJRzltSUhaaGJIVmxJR2x1ZEc4Z2RHaGxJRzV6WEc0Z1hIUXZMeUJ0YjJSbElDWWdORG9nY21WMGRYSnVJSFpoYkhWbElIZG9aVzRnWVd4eVpXRmtlU0J1Y3lCdlltcGxZM1JjYmlCY2RDOHZJRzF2WkdVZ0ppQTRmREU2SUdKbGFHRjJaU0JzYVd0bElISmxjWFZwY21WY2JpQmNkRjlmZDJWaWNHRmphMTl5WlhGMWFYSmxYMTh1ZENBOUlHWjFibU4wYVc5dUtIWmhiSFZsTENCdGIyUmxLU0I3WEc0Z1hIUmNkR2xtS0cxdlpHVWdKaUF4S1NCMllXeDFaU0E5SUY5ZmQyVmljR0ZqYTE5eVpYRjFhWEpsWDE4b2RtRnNkV1VwTzF4dUlGeDBYSFJwWmlodGIyUmxJQ1lnT0NrZ2NtVjBkWEp1SUhaaGJIVmxPMXh1SUZ4MFhIUnBaaWdvYlc5a1pTQW1JRFFwSUNZbUlIUjVjR1Z2WmlCMllXeDFaU0E5UFQwZ0oyOWlhbVZqZENjZ0ppWWdkbUZzZFdVZ0ppWWdkbUZzZFdVdVgxOWxjMDF2WkhWc1pTa2djbVYwZFhKdUlIWmhiSFZsTzF4dUlGeDBYSFIyWVhJZ2JuTWdQU0JQWW1wbFkzUXVZM0psWVhSbEtHNTFiR3dwTzF4dUlGeDBYSFJmWDNkbFluQmhZMnRmY21WeGRXbHlaVjlmTG5Jb2JuTXBPMXh1SUZ4MFhIUlBZbXBsWTNRdVpHVm1hVzVsVUhKdmNHVnlkSGtvYm5Nc0lDZGtaV1poZFd4MEp5d2dleUJsYm5WdFpYSmhZbXhsT2lCMGNuVmxMQ0IyWVd4MVpUb2dkbUZzZFdVZ2ZTazdYRzRnWEhSY2RHbG1LRzF2WkdVZ0ppQXlJQ1ltSUhSNWNHVnZaaUIyWVd4MVpTQWhQU0FuYzNSeWFXNW5KeWtnWm05eUtIWmhjaUJyWlhrZ2FXNGdkbUZzZFdVcElGOWZkMlZpY0dGamExOXlaWEYxYVhKbFgxOHVaQ2h1Y3l3Z2EyVjVMQ0JtZFc1amRHbHZiaWhyWlhrcElIc2djbVYwZFhKdUlIWmhiSFZsVzJ0bGVWMDdJSDB1WW1sdVpDaHVkV3hzTENCclpYa3BLVHRjYmlCY2RGeDBjbVYwZFhKdUlHNXpPMXh1SUZ4MGZUdGNibHh1SUZ4MEx5OGdaMlYwUkdWbVlYVnNkRVY0Y0c5eWRDQm1kVzVqZEdsdmJpQm1iM0lnWTI5dGNHRjBhV0pwYkdsMGVTQjNhWFJvSUc1dmJpMW9ZWEp0YjI1NUlHMXZaSFZzWlhOY2JpQmNkRjlmZDJWaWNHRmphMTl5WlhGMWFYSmxYMTh1YmlBOUlHWjFibU4wYVc5dUtHMXZaSFZzWlNrZ2UxeHVJRngwWEhSMllYSWdaMlYwZEdWeUlEMGdiVzlrZFd4bElDWW1JRzF2WkhWc1pTNWZYMlZ6VFc5a2RXeGxJRDljYmlCY2RGeDBYSFJtZFc1amRHbHZiaUJuWlhSRVpXWmhkV3gwS0NrZ2V5QnlaWFIxY200Z2JXOWtkV3hsV3lka1pXWmhkV3gwSjEwN0lIMGdPbHh1SUZ4MFhIUmNkR1oxYm1OMGFXOXVJR2RsZEUxdlpIVnNaVVY0Y0c5eWRITW9LU0I3SUhKbGRIVnliaUJ0YjJSMWJHVTdJSDA3WEc0Z1hIUmNkRjlmZDJWaWNHRmphMTl5WlhGMWFYSmxYMTh1WkNoblpYUjBaWElzSUNkaEp5d2daMlYwZEdWeUtUdGNiaUJjZEZ4MGNtVjBkWEp1SUdkbGRIUmxjanRjYmlCY2RIMDdYRzVjYmlCY2RDOHZJRTlpYW1WamRDNXdjbTkwYjNSNWNHVXVhR0Z6VDNkdVVISnZjR1Z5ZEhrdVkyRnNiRnh1SUZ4MFgxOTNaV0p3WVdOclgzSmxjWFZwY21WZlh5NXZJRDBnWm5WdVkzUnBiMjRvYjJKcVpXTjBMQ0J3Y205d1pYSjBlU2tnZXlCeVpYUjFjbTRnVDJKcVpXTjBMbkJ5YjNSdmRIbHdaUzVvWVhOUGQyNVFjbTl3WlhKMGVTNWpZV3hzS0c5aWFtVmpkQ3dnY0hKdmNHVnlkSGtwT3lCOU8xeHVYRzRnWEhRdkx5QmZYM2RsWW5CaFkydGZjSFZpYkdsalgzQmhkR2hmWDF4dUlGeDBYMTkzWldKd1lXTnJYM0psY1hWcGNtVmZYeTV3SUQwZ1hDSmNJanRjYmx4dVhHNGdYSFF2THlCTWIyRmtJR1Z1ZEhKNUlHMXZaSFZzWlNCaGJtUWdjbVYwZFhKdUlHVjRjRzl5ZEhOY2JpQmNkSEpsZEhWeWJpQmZYM2RsWW5CaFkydGZjbVZ4ZFdseVpWOWZLRjlmZDJWaWNHRmphMTl5WlhGMWFYSmxYMTh1Y3lBOUlEQXBPMXh1SWl3aUx5OHRMUzB0TFNCaWRYUjBiMjRnZEc5d0lDMHRMUzB0THk5Y2NseHVZMjl1YzNRZ1luUnVWRzl3SUQwZ1pHOWpkVzFsYm5RdWNYVmxjbmxUWld4bFkzUnZjaWduTG1KMGJsOWZkRzl3SnlrN1hISmNibHh5WEc0dkx5QWdZblYwZEc5dUlIUnZjQ0J6YUc5M1hISmNibmRwYm1SdmR5NWhaR1JGZG1WdWRFeHBjM1JsYm1WeUtDZHpZM0p2Ykd3bkxDQW9LU0E5UGlCN1hISmNiaUFnSUNCM2FXNWtiM2N1Y0dGblpWbFBabVp6WlhRZ1BpQXhNREF3SUQ4Z1luUnVWRzl3TG1Oc1lYTnpUR2x6ZEM1aFpHUW9KMkowYmw5ZllXTjBhWFpsSnlrZ09pQmlkRzVVYjNBdVkyeGhjM05NYVhOMExuSmxiVzkyWlNnblluUnVYMTloWTNScGRtVW5LVHRjY2x4dWZTazdYSEpjYmx4eVhHNHZMeUJpZFhSMGIyNGdZMnhwWTJzZ2RHOXdYSEpjYm1KMGJsUnZjQzVoWkdSRmRtVnVkRXhwYzNSbGJtVnlLQ2RqYkdsamF5Y3NJQ2dwSUQwK0lIZHBibVJ2ZHk1elkzSnZiR3dvTUN3Z01Da3BPeUlzSWk4dkxTMHRMUzBnYm1GMlltRnlJQ3NnWW5WeVoyVnlJR0ZqZEdsMlpTQXRMUzB0TFM4dlhISmNibU52Ym5OMElHSjFjbWRsY2lBZ0lDQWdJRDBnWkc5amRXMWxiblF1Y1hWbGNubFRaV3hsWTNSdmNpZ25MbWhsWVdSbGNsOWZZblZ5WjJWeUp5a3NYSEpjYmlBZ0lDQWdJRzVoZG1KaGNpQWdJQ0FnSUQwZ1pHOWpkVzFsYm5RdWNYVmxjbmxUWld4bFkzUnZjaWduTG1obFlXUmxjbDlmYm1GMlltRnlKeWtzWEhKY2JpQWdJQ0FnSUhCdmNIVndUbUYyWW1GeUlEMGdaRzlqZFcxbGJuUXVjWFZsY25sVFpXeGxZM1J2Y2lnbkxtaGxZV1JsY2w5ZmNHOXdkWEFuS1R0Y2NseHVMeTltZFc1amRHbHZiaUJ2Y0dWdUlHMWxiblZjY2x4dVkyOXVjM1FnYjNCbGJrMWxiblVnUFNBb0tTQTlQaUI3WEhKY2JpQWdJQ0JpZFhKblpYSXVZMnhoYzNOTWFYTjBMblJ2WjJkc1pTZ25ZblZ5WjJWeVgxOWhZM1JwZG1VbktUdGNjbHh1SUNBZ0lHNWhkbUpoY2k1amJHRnpjMHhwYzNRdWRHOW5aMnhsS0NkdVlYWmlZWEpmWDJGamRHbDJaU2NwTzF4eVhHNGdJQ0FnY0c5d2RYQk9ZWFppWVhJdVkyeGhjM05NYVhOMExuUnZaMmRzWlNnbmNHOXdkWEJmWDJGamRHbDJaU2NwTzF4eVhHNTlYSEpjYmx4eVhHNXdiM0IxY0U1aGRtSmhjaTVoWkdSRmRtVnVkRXhwYzNSbGJtVnlLQ2RqYkdsamF5Y3NJRzl3Wlc1TlpXNTFLVHRjY2x4dVluVnlaMlZ5TG1Ga1pFVjJaVzUwVEdsemRHVnVaWElvSjJOc2FXTnJKeXdnYjNCbGJrMWxiblVwT3lJc0lpOHZMUzB0TFMwZ2JXVmthV0VnZDJsdVpHOTNJQzB0TFMwdEx5OWNjbHh1WTI5dWMzUWdiV1ZrYVdFZ1BTQmpiMjVrYVhScGIyNGdQVDRnS0hkcGJtUnZkeTV0WVhSamFFMWxaR2xoS0dOdmJtUnBkR2x2YmlrdWJXRjBZMmhsY3lrN1hISmNibHh5WEc0dkx5MHRMUzB0SUhCeWIycGxZM1J6SUhOc2FXUmxjaUF0TFMwdExTOHZYSEpjYm1OdmJuTjBJSEJ5YjJwbFkzUlVjbUZqYXlBOUlHUnZZM1Z0Wlc1MExuRjFaWEo1VTJWc1pXTjBiM0lvSnk1d2NtOXFaV04wYzE5ZmRISmhZMnNuS1N4Y2NseHVJQ0FnSUNBZ2NISnZhbVZqZEZCeVpYWWdJRDBnWkc5amRXMWxiblF1Y1hWbGNubFRaV3hsWTNSdmNpZ25MbkJ5YjJwbFkzUnpYMTl3Y21WMkp5a3NYSEpjYmlBZ0lDQWdJSEJ5YjJwbFkzUk9aWGgwSUNBOUlHUnZZM1Z0Wlc1MExuRjFaWEo1VTJWc1pXTjBiM0lvSnk1d2NtOXFaV04wYzE5ZmJtVjRkQ2NwTzF4eVhHNHZMeUJ3YjNOcGRHbHZiaUIwY21GamExeHlYRzVzWlhRZ2NISnZhbVZqZEZCeklEMGdNRHRjY2x4dUx5OXVaWGgwWEhKY2JtTnZibk4wSUc1bGVIUlFjbTlxWldOMElEMGdLQ2tnUFQ0Z2UxeHlYRzRnSUNBZ2FXWW9iV1ZrYVdFb1hDSW9iV2x1TFhkcFpIUm9PaUF4TWpBd2NIZ3BYQ0lwS1h0Y2NseHVJQ0FnSUNBZ0lDQndjbTlxWldOMFVITWdQVDBnTVRFME1DQXFJRElnUHlCd2NtOXFaV04wVUhNZ1BTQXdJRG9nY0hKdmFtVmpkRkJ6SUNzOUlERXhOREE3WEhKY2JpQWdJQ0I5WEhKY2JpQWdJQ0JsYkhObElHbG1LRzFsWkdsaEtGd2lLRzFwYmkxM2FXUjBhRG9nTlRjMmNIZ3BYQ0lwS1h0Y2NseHVJQ0FnSUNBZ0lDQndjbTlxWldOMFVITWdQVDBnTlRjd0lDb2dOU0EvSUhCeWIycGxZM1JRY3lBOUlEQWdPaUJ3Y205cVpXTjBVSE1nS3owZ05UY3dPMXh5WEc0Z0lDQWdmVnh5WEc0Z0lDQWdaV3h6Wlh0Y2NseHVJQ0FnSUNBZ0lDQndjbTlxWldOMFVITWdQVDBnTXpBd0lDb2dOU0EvSUhCeWIycGxZM1JRY3lBOUlEQWdPaUJ3Y205cVpXTjBVSE1nS3owZ016QXdPMXh5WEc0Z0lDQWdmVnh5WEc0Z0lDQWdjSEp2YW1WamRGUnlZV05yTG5OMGVXeGxMblJ5WVc1elptOXliU0E5SUdCMGNtRnVjMnhoZEdVb0xTUjdjSEp2YW1WamRGQnpmWEI0S1dBN1hISmNibjFjY2x4dUx5OXdjbVYyWEhKY2JtTnZibk4wSUhCeVpYWlFjbTlxWldOMElEMGdLQ2tnUFQ0Z2UxeHlYRzRnSUNBZ2FXWW9iV1ZrYVdFb1hDSW9iV2x1TFhkcFpIUm9PaUF4TWpBd2NIZ3BYQ0lwS1h0Y2NseHVJQ0FnSUNBZ0lDQndjbTlxWldOMFVITWdQVDBnTUNBL0lIQnliMnBsWTNSUWN5QTlJREV4TkRBZ0tpQXlJRG9nY0hKdmFtVmpkRkJ6SUMwOUlERXhOREE3WEhKY2JpQWdJQ0I5WEhKY2JpQWdJQ0JsYkhObElHbG1LRzFsWkdsaEtGd2lLRzFwYmkxM2FXUjBhRG9nTlRjMmNIZ3BYQ0lwS1h0Y2NseHVJQ0FnSUNBZ0lDQndjbTlxWldOMFVITWdQVDBnTUNBL0lIQnliMnBsWTNSUWN5QTlJRFUzTUNBcUlEVWdPaUJ3Y205cVpXTjBVSE1nTFQwZ05UY3dPMXh5WEc0Z0lDQWdmVnh5WEc0Z0lDQWdaV3h6Wlh0Y2NseHVJQ0FnSUNBZ0lDQndjbTlxWldOMFVITWdQVDBnTUNBL0lIQnliMnBsWTNSUWN5QTlJRE13TUNBcUlEVWdPaUJ3Y205cVpXTjBVSE1nTFQwZ016QXdPMXh5WEc0Z0lDQWdmVnh5WEc0Z0lDQWdjSEp2YW1WamRGUnlZV05yTG5OMGVXeGxMblJ5WVc1elptOXliU0E5SUdCMGNtRnVjMnhoZEdVb0xTUjdjSEp2YW1WamRGQnpmWEI0S1dBN1hISmNibjFjY2x4dVhISmNibkJ5YjJwbFkzUk9aWGgwTG1Ga1pFVjJaVzUwVEdsemRHVnVaWElvSjJOc2FXTnJKeXdnYm1WNGRGQnliMnBsWTNRcE8xeHlYRzV3Y205cVpXTjBVSEpsZGk1aFpHUkZkbVZ1ZEV4cGMzUmxibVZ5S0NkamJHbGpheWNzSUhCeVpYWlFjbTlxWldOMEtUdGNjbHh1SWwwc0luTnZkWEpqWlZKdmIzUWlPaUlpZlE9PSJ9
